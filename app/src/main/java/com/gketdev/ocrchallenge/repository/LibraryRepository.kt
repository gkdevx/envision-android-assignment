package com.gketdev.ocrchallenge.repository

import com.gketdev.ocrchallenge.data.Library
import com.gketdev.ocrchallenge.data.Result
import com.gketdev.ocrchallenge.source.DataSourceMapper
import com.gketdev.ocrchallenge.source.LocalDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class LibraryRepository @Inject constructor(private val localDataSource: LocalDataSource) {

    fun getSavedTexts(): Flow<Result<List<Library>>> = flow {
        localDataSource.savedTexts.collect {
            emit(DataSourceMapper.mapData(it))
        }
    }
}

