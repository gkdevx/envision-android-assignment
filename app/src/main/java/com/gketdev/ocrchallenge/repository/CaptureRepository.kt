package com.gketdev.ocrchallenge.repository

import com.gketdev.ocrchallenge.source.LocalDataSource
import com.gketdev.ocrchallenge.source.OcrDataSource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

class CaptureRepository @Inject constructor(
    private val dataSource: OcrDataSource,
    private val localDataSource: LocalDataSource
) {

    fun fetchParagraphs(file: File) = flow {
        dataSource.readDataFromDocument(file).collect {
            emit(it)
        }
    }

    fun saveText(text: String) {
        localDataSource.addItem(text)
    }

}