package com.gketdev.ocrchallenge.ui.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.gketdev.ocrchallenge.R
import com.gketdev.ocrchallenge.adapter.OcrPagerAdapter
import com.gketdev.ocrchallenge.databinding.ActivityMainBinding
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpViewPager()
        observeItems()
    }

    private fun setUpViewPager() {
        binding.viewPager.adapter = OcrPagerAdapter(this)
        val viewPager = binding.viewPager
        val tabLayout = binding.tabLayout
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = getString(R.string.text_heading_capture)
                    tab.contentDescription = getString(R.string.text_heading_capture)
                }
                1 -> {
                    tab.text = getString(R.string.text_heading_library)
                    tab.contentDescription =
                        getString(R.string.text_description_heading_library)
                }
            }
        }.attach()
    }

    private fun observeItems() = lifecycleScope.launchWhenCreated {
        viewModel.viewState.collect {
            when (it) {
                is MainViewState.CurrentTab -> {
                    binding.viewPager.setCurrentItem(it.tabPosition, false)
                }
            }
        }
    }


}