package com.gketdev.ocrchallenge.ui.paragraph

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gketdev.ocrchallenge.ui.main.MainViewModel
import com.gketdev.ocrchallenge.R
import com.gketdev.ocrchallenge.base.BaseFragment
import com.gketdev.ocrchallenge.databinding.FragmentCaptureDetailBinding
import com.gketdev.ocrchallenge.ui.main.MainViewState
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class CaptureDetailFragment : BaseFragment<FragmentCaptureDetailBinding>() {

    private val args by navArgs<CaptureDetailFragmentArgs>()
    private var paragraph = ""
    private val viewModel: CaptureDetailViewModel by viewModels()
    private val mainViewModel: MainViewModel by activityViewModels()

    override fun initViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToParent: Boolean
    ): FragmentCaptureDetailBinding {
        return FragmentCaptureDetailBinding.inflate(inflater, container, attachToParent)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initListener()
    }

    private fun initUi() {
        paragraph = args.paragraph
        binding?.textViewParagraph?.text = paragraph
        binding?.textViewParagraph?.movementMethod = ScrollingMovementMethod()
    }

    private fun initListener() {
        binding?.buttonSaveText?.setOnClickListener {
            binding?.buttonSaveText?.isClickable = false
            viewModel.addItem(paragraph)
            val snackbar = binding?.root?.let { it1 ->
                Snackbar.make(
                    it1,
                    R.string.text_saved, Snackbar.LENGTH_SHORT
                )
            }
            snackbar?.setAction(
                getString(R.string.text_go_to_library)
            ) {
                findNavController().navigate(R.id.captureFragment)
                mainViewModel.switchTabToLibrary()
            }
            snackbar?.view?.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
            snackbar?.show()
        }
    }

}