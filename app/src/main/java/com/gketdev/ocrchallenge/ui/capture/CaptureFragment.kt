package com.gketdev.ocrchallenge.ui.capture

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.gketdev.ocrchallenge.R
import com.gketdev.ocrchallenge.base.BaseFragment
import com.gketdev.ocrchallenge.data.Paragraph
import com.gketdev.ocrchallenge.databinding.FragmentCaptureBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class CaptureFragment : BaseFragment<FragmentCaptureBinding>() {

    private val requiredPermissions = arrayOf(Manifest.permission.CAMERA)
    private lateinit var activityResultLauncher: ActivityResultLauncher<String>
    private var imageCapture: ImageCapture? = null
    private val viewModel: CaptureViewModel by viewModels()

    override fun initViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToParent: Boolean
    ): FragmentCaptureBinding {
        return FragmentCaptureBinding.inflate(inflater, container, attachToParent)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkPermission()
        onObserveData()
        onListener()
    }

    private fun checkPermission() {
        activityResultLauncher = registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                startCamera()
            } else {
                activityResultLauncher.launch(
                    Manifest.permission.CAMERA
                )
            }
        }

        if (allPermissionsGranted()) {
            startCamera()
        } else {
            activityResultLauncher.launch(
                Manifest.permission.CAMERA
            )
        }
    }

    private fun allPermissionsGranted() = requiredPermissions.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener({
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
            binding?.previewView?.implementationMode = PreviewView.ImplementationMode.COMPATIBLE
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(binding?.previewView?.surfaceProvider)
                }
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
            imageCapture = ImageCapture.Builder()
                .build()
            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture);
            } catch (exc: Exception) {
                Log.e("ERROR:", "Use case binding failed", exc)
            }
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return

        val photoFile = File(
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            SimpleDateFormat(
                "yyyy-mm-dd", Locale.US
            ).format(System.currentTimeMillis()) + ".jpg"
        )

        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.d("CAPTURE:::", "Error + ${exc.localizedMessage}")
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    viewModel.sendPhoto(photoFile)
                }
            })
    }

    private fun onListener() {
        binding?.buttonCapture?.setOnClickListener {
            takePhoto()
        }
    }

    private fun onObserveData() {
        lifecycleScope.launchWhenCreated {
            viewModel.viewState.collect {
                when (it) {
                    is CaptureViewState.Paragraphs -> {
                        val action =
                            CaptureFragmentDirections.actionCaptureFragmentToDetailFragment(
                                (processParagraphs(it.paragraphs))
                            )
                        findNavController().navigate(action)
                    }
                    is CaptureViewState.Error -> {
                        binding?.textViewOcrLoading?.visibility = View.GONE
                        binding?.buttonCapture?.visibility = View.VISIBLE
                        Toast.makeText(requireContext(), it.error, Toast.LENGTH_LONG).show()
                        binding?.buttonCapture?.isClickable = true
                    }
                    is CaptureViewState.EmptyData -> {
                        binding?.textViewOcrLoading?.visibility = View.GONE
                        binding?.buttonCapture?.visibility = View.VISIBLE
                        Toast.makeText(
                            requireContext(),
                            getString(R.string.text_not_found_paragraph),
                            Toast.LENGTH_LONG
                        ).show()
                        binding?.buttonCapture?.isClickable = true
                    }
                    is CaptureViewState.RequestLoading -> {
                        binding?.textViewOcrLoading?.visibility = View.VISIBLE
                        binding?.buttonCapture?.visibility = View.GONE
                        binding?.buttonCapture?.isClickable = false
                    }
                }
            }
        }
    }

    private fun processParagraphs(paragraph: List<Paragraph>): String {
        var text = ""
        paragraph.forEach {
            text += "\t${it.paragraph}\n \n"
        }
        return text
    }


}