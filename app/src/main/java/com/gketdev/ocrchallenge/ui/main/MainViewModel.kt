package com.gketdev.ocrchallenge.ui.main

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainViewModel : ViewModel() {

    private val _viewState = MutableStateFlow<MainViewState>(MainViewState.CurrentTab(1))
    val viewState: StateFlow<MainViewState> = _viewState

    fun switchTabToLibrary() {
        _viewState.value = MainViewState.CurrentTab(1)
    }

}