package com.gketdev.ocrchallenge.ui.capture

import com.gketdev.ocrchallenge.data.Paragraph

sealed class CaptureViewState {
    object Loading : CaptureViewState()
    object RequestLoading : CaptureViewState()
    data class Paragraphs(val paragraphs: List<Paragraph>) : CaptureViewState()
    data class Error(val error: String?) : CaptureViewState()
    object EmptyData : CaptureViewState()
}

