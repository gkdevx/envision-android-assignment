package com.gketdev.ocrchallenge.ui.main

sealed class MainViewState {
    class CurrentTab(val tabPosition: Int) : MainViewState()
}
