package com.gketdev.ocrchallenge.ui.library

import com.gketdev.ocrchallenge.data.Library

sealed class LibraryViewState {
    object Loading : LibraryViewState()
    data class LibraryItems(val libraryItems: List<Library>) : LibraryViewState()
    data class Error(val error: String?) : LibraryViewState()
    object EmptyData : LibraryViewState()
}
