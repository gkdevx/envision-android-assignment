package com.gketdev.ocrchallenge.ui.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.gketdev.ocrchallenge.adapter.LibraryAdapter
import com.gketdev.ocrchallenge.base.BaseFragment
import com.gketdev.ocrchallenge.databinding.FragmentLibraryBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@AndroidEntryPoint
class LibraryFragment : BaseFragment<FragmentLibraryBinding>() {

    private val viewModel: LibraryViewModel by viewModels()

    @Inject
    lateinit var adapter: LibraryAdapter

    override fun initViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToParent: Boolean
    ): FragmentLibraryBinding {
        return FragmentLibraryBinding.inflate(inflater, container, attachToParent)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeItems()
    }

    private fun initView() {
        binding?.recyclerView?.adapter = adapter

        adapter.onItemClicked = {

        }
    }

    private fun observeItems() {
        lifecycleScope.launchWhenCreated {
            viewModel.viewState.collect {
                when (it) {
                    is LibraryViewState.LibraryItems -> {
                        adapter.libraryItems = it.libraryItems
                        binding?.textViewEmptyData?.visibility = View.GONE
                        binding?.recyclerView?.visibility = View.VISIBLE
                    }
                    is LibraryViewState.Error -> {
                        binding?.textViewEmptyData?.visibility = View.GONE
                        Toast.makeText(requireContext(), it.error.toString(), Toast.LENGTH_SHORT)
                            .show()
                        binding?.recyclerView?.visibility = View.VISIBLE
                    }
                    is LibraryViewState.EmptyData -> {
                        binding?.textViewEmptyData?.visibility = View.VISIBLE
                        binding?.recyclerView?.visibility = View.GONE
                    }
                }
            }
        }
    }

}