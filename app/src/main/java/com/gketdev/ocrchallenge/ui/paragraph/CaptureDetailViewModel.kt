package com.gketdev.ocrchallenge.ui.paragraph

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gketdev.ocrchallenge.repository.CaptureRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CaptureDetailViewModel @Inject constructor(private val repository: CaptureRepository) :
    ViewModel() {

    fun addItem(paragraph: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.saveText(paragraph)
        }
    }

}