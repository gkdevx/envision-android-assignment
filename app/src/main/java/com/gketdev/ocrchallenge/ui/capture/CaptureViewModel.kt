package com.gketdev.ocrchallenge.ui.capture

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gketdev.ocrchallenge.data.Result
import com.gketdev.ocrchallenge.repository.CaptureRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject

@HiltViewModel
class CaptureViewModel @Inject constructor(private val repository: CaptureRepository) :
    ViewModel() {

    private val _viewState = MutableStateFlow<CaptureViewState>(CaptureViewState.Loading)
    val viewState: StateFlow<CaptureViewState> = _viewState

    fun sendPhoto(file: File) {
        _viewState.value = CaptureViewState.RequestLoading
        viewModelScope.launch {
            repository.fetchParagraphs(file).collect {
                when (it) {
                    is Result.Error -> {
                        _viewState.value = CaptureViewState.Error(it.message)
                    }
                    is Result.Success -> {
                        if (it.data.isNullOrEmpty()) {
                            _viewState.value = CaptureViewState.EmptyData
                        } else {
                            _viewState.value = CaptureViewState.Paragraphs(it.data)
                        }
                    }
                }
            }
        }
    }

}