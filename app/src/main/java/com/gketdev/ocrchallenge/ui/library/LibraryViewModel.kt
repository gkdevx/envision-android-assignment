package com.gketdev.ocrchallenge.ui.library

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gketdev.ocrchallenge.data.ErrorCode
import com.gketdev.ocrchallenge.data.Result
import com.gketdev.ocrchallenge.repository.LibraryRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LibraryViewModel @Inject constructor(private val repository: LibraryRepository) :
    ViewModel() {

    private val _viewState = MutableStateFlow<LibraryViewState>(LibraryViewState.Loading)
    val viewState: StateFlow<LibraryViewState> = _viewState

    init {
        getSavedTexts()
    }

    private fun getSavedTexts() {
        viewModelScope.launch {
            repository.getSavedTexts().collect {
                when (it) {
                    is Result.Error -> {
                        if (it.code == ErrorCode.EMPTY_LIST_ERROR) {
                            _viewState.value = LibraryViewState.EmptyData
                        } else {
                            _viewState.value = LibraryViewState.Error(it.message)
                        }
                    }
                    is Result.Success -> {
                        _viewState.value = LibraryViewState.LibraryItems(it.data)
                    }
                }
            }
        }
    }

}