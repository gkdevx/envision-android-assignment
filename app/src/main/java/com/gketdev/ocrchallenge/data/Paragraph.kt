package com.gketdev.ocrchallenge.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Paragraph(
    @Json(name = "paragraph")
    val paragraph : String?,
    @Json(name = "language")
    val language : String?
)
