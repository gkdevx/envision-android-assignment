package com.gketdev.ocrchallenge.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "library")
data class Library(
    @PrimaryKey(autoGenerate = true)
    val paragraphId: Int = 0,
    val savedTime: Long,
    val content: String
)