package com.gketdev.ocrchallenge.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GeneralResponse(
    @Json(name = "response")
    val response : ParagraphResponse
)
