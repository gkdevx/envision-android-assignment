package com.gketdev.ocrchallenge.data

enum class ErrorCode {
    EMPTY_LIST_ERROR,
    NULL_LIST_ERROR
}