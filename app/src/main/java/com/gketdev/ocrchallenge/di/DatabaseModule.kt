package com.gketdev.ocrchallenge.di

import android.content.Context
import androidx.room.Room
import com.gketdev.ocrchallenge.R
import com.gketdev.ocrchallenge.database.LibraryDao
import com.gketdev.ocrchallenge.database.LibraryDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): LibraryDatabase {
        return Room.databaseBuilder(
            appContext,
            LibraryDatabase::class.java,
            appContext.getString(R.string.app_name)
        ).build()
    }

    @Provides
    @Singleton
    fun provideDao(database: LibraryDatabase): LibraryDao {
        return database.getLibraryDao()
    }
}