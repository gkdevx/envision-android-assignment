package com.gketdev.ocrchallenge.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gketdev.ocrchallenge.HostFragment
import com.gketdev.ocrchallenge.ui.library.LibraryFragment

class OcrPagerAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> HostFragment()
            1 -> LibraryFragment()
            else -> LibraryFragment()
        }
    }
}