package com.gketdev.ocrchallenge.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gketdev.ocrchallenge.data.Library
import com.gketdev.ocrchallenge.databinding.ItemLibraryBinding
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.properties.Delegates

class LibraryAdapter @Inject constructor() :
    RecyclerView.Adapter<LibraryAdapter.LibraryViewHolder>() {

    var onItemClicked: ((Library) -> Unit) = {}

    var libraryItems: List<Library> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibraryViewHolder {
        val itemBinding =
            ItemLibraryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LibraryViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: LibraryViewHolder, position: Int) {
        val libraryByPositioned = libraryItems[position]
        holder.bind(libraryByPositioned)
    }

    override fun getItemCount() = libraryItems.size


    inner class LibraryViewHolder(private val binding: ItemLibraryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Library) {
            val format = SimpleDateFormat("dd/MM/yy HH:mm", Locale.getDefault())
            val date = Date(item.savedTime)
            val resultText = format.format(date)
            binding.textViewHeading.text = resultText
            binding.root.setOnClickListener {
                onItemClicked.invoke(item)
            }
        }
    }



}
