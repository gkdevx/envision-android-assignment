package com.gketdev.ocrchallenge

import android.view.LayoutInflater
import android.view.ViewGroup
import com.gketdev.ocrchallenge.base.BaseFragment
import com.gketdev.ocrchallenge.databinding.FragmentHostBinding

class HostFragment : BaseFragment<FragmentHostBinding>() {

    override fun initViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToParent: Boolean
    ): FragmentHostBinding {
        return FragmentHostBinding.inflate(inflater, container, attachToParent)
    }

}