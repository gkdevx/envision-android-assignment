package com.gketdev.ocrchallenge.base

import com.gketdev.ocrchallenge.data.Result

abstract class BaseCallMapper {
    protected suspend fun <T : Any> apiCallResponse(call: suspend () -> T): Result<T> {
        return try {
            val response = call()
            Result.Success(response)
        } catch (exception: Throwable) {
            Result.Error(error = exception, message = exception.message.toString())
        }
    }

}