package com.gketdev.ocrchallenge

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OcrApp : Application() {
}