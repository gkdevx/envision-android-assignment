package com.gketdev.ocrchallenge.source

import com.gketdev.ocrchallenge.data.Library
import com.gketdev.ocrchallenge.data.Result
import com.gketdev.ocrchallenge.data.ErrorCode

object DataSourceMapper {
    fun mapData(data: List<Library>?): Result<List<Library>> {
        return when {
            data == null -> {
                Result.Error(
                    message = "Null",
                    code = ErrorCode.NULL_LIST_ERROR
                )
            }
            data.isEmpty() -> {
                Result.Error(
                    message = "Empty List",
                    code = ErrorCode.EMPTY_LIST_ERROR
                )
            }
            else -> {
                Result.Success(data)
            }
        }
    }
}