package com.gketdev.ocrchallenge.source

import com.gketdev.ocrchallenge.data.Library
import com.gketdev.ocrchallenge.database.LibraryDao
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class LocalDataSource @Inject constructor(private val dao: LibraryDao) {

    val savedTexts = flow {
        dao.getSavedTexts()
            .catch { emit(null) }
            .collect {
                emit(it)
            }
    }

    fun addItem(text: String) {
        val library = Library(
            savedTime = System.currentTimeMillis(),
            content = text
        )
        dao.addText(library)
    }

}