package com.gketdev.ocrchallenge.source

import com.gketdev.ocrchallenge.api.OcrApiService
import com.gketdev.ocrchallenge.base.BaseCallMapper
import com.gketdev.ocrchallenge.data.Paragraph
import com.gketdev.ocrchallenge.data.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject


class OcrDataSource @Inject constructor(private val ocrApiService: OcrApiService) :
    BaseCallMapper() {

    fun readDataFromDocument(file: File): Flow<Result<List<Paragraph>>> = flow {
        val body: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                "photo",
                file.name,
                file.asRequestBody("image/*".toMediaType())
            )
        emit(apiCallResponse {
            ocrApiService.readDocument(body).response.paragraphs
        })
    }
}


