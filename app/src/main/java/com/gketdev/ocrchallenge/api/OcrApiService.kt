package com.gketdev.ocrchallenge.api

import com.gketdev.ocrchallenge.data.GeneralResponse
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface OcrApiService {
    @Multipart
    @POST("api/test/readDocument")
    suspend fun readDocument(@Part file: MultipartBody.Part): GeneralResponse
}