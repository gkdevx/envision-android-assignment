package com.gketdev.ocrchallenge.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gketdev.ocrchallenge.data.Library

@Database(entities = [Library::class], version = 1, exportSchema = false)
abstract class LibraryDatabase : RoomDatabase() {
    abstract fun getLibraryDao(): LibraryDao
}