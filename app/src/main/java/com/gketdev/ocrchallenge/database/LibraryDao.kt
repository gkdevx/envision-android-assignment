package com.gketdev.ocrchallenge.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.gketdev.ocrchallenge.data.Library
import kotlinx.coroutines.flow.Flow

@Dao
interface LibraryDao {

    @Query("SELECT * FROM library")
    fun getSavedTexts(): Flow<List<Library>>

    @Insert
    fun addText(item: Library)

}